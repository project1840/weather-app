import { render, screen, fireEvent } from "@testing-library/react";
import { act } from "react-dom/test-utils";
import axios from "axios";
import App from "./App";
import { mockApiData, mockPositionByApi, mockCityInfo } from "./testData";

jest.mock("axios");

describe("App component", () => {
  it("renders App name", async () => {
    await axios.get.mockResolvedValueOnce(mockCityInfo);
    await axios.get.mockResolvedValueOnce(mockPositionByApi);
    await axios.get.mockResolvedValueOnce(mockApiData);
    render(<App />);
    const result = await screen.findByText("Weather App");
    expect(result.className).toBe("title");
  });
});

describe("search functionality", () => {
  it("should call axios on search", async () => {
    await axios.get.mockResolvedValueOnce(mockCityInfo);
    await axios.get.mockResolvedValueOnce(mockPositionByApi);
    await axios.get.mockResolvedValueOnce(mockApiData);
    render(<App />);
    await axios.get.mockResolvedValueOnce(mockPositionByApi);
    await axios.get.mockResolvedValueOnce(mockApiData);
    await act(async () => {
      fireEvent.change(screen.getByPlaceholderText("Search here"), {
        target: { value: "Kolkata" },
      });
      fireEvent.click(screen.getByText(/Search/i));
    });
    expect(axios.get).toBeCalled();
  });
});

describe("unit change", () => {
  it("renders unit changes", async () => {
    await axios.get.mockResolvedValueOnce(mockCityInfo);
    await axios.get.mockResolvedValueOnce(mockPositionByApi);
    await axios.get.mockResolvedValueOnce(mockApiData);
    render(<App />);
    await act(async () => {
      fireEvent.click(screen.getByTestId("unit"));
    });
    expect(screen.findByText("Change to °C")).toBeTruthy();
    await act(async () => {
      fireEvent.click(screen.getByTestId("unit"));
    });
    expect(screen.findByText("Change to °F")).toBeTruthy();
  });
});
