export const LON_LAT_API='https://ipinfo.io/json?token=';
export const WEATHER_FORECAST_API='https://api.openweathermap.org/data/2.5/forecast?';
export const COORDINATES_API='https://api.openweathermap.org/data/2.5/weather?q=location';