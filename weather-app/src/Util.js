export const celsiusToFahrenheit = (value) => (1.8 * value + 32).toFixed(2);

export const getForcastData = (forecastResponse) => {
  let dataForEachDay = [];
  for (
    let i = 0;
    i < forecastResponse?.data?.list?.length;
    i += forecastResponse?.data?.list?.length / 5
  ) {
    dataForEachDay.push(forecastResponse.data.list[i]);
  }
  let cityForecast = {};
  cityForecast.dataForEachDay = dataForEachDay;
  cityForecast.cityName = forecastResponse?.data?.city?.name;
  return cityForecast;
};

const days = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wed-nesday",
  "Thursday",
  "Friday",
  "Saturday",
];
const months = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];

export const getDate = (value) => {
  let currentDate = new Date(value);
  let date = Number(currentDate.getDate());
  let day = currentDate.getDay();
  let month = currentDate.getMonth();
  if (date > 3 && date < 21) {
    date = date + "th";
  } else {
    switch (date % 10) {
      case 1:
        date = date + "st";
        break;
      case 2:
        date = date + "nd";
        break;
      case 3:
        date = date + "rd";
        break;
      default:
        date = date + "th";
    }
  }
  let formatdate = `${days[day]?.replace("-","")}, ${months[month]} ${date}`;
  console.log(formatdate,"formatdate--")
  return formatdate;
};

export const getDay = (value) => {
  console.log(value,"date")
  const currentDate = new Date(value);
  const day = currentDate.getDay();
  const currentDay = new Date().getDay();
  if (currentDay === day) {
    return "Today";
  }
  return days[day];
};
