import React, { useEffect, useState } from "react";
import DayCard from "./DayCard";
import { getWeatherForecast, getCoordinates, getLonLat } from "../Service";
import { celsiusToFahrenheit, getDate, getForcastData } from "../Util";

const WeatherPage = () => {
  const [parameters, setParameters] = useState({
    weatherData: [],
    isCelsius: true,
    isLoading: true,
    isError: false,
    location: "",
  });
  const dailyForecast = async (location) => {
    try {
      const responseData = await getCoordinates(location);
      const { lon, lat } = responseData?.data?.coord;
      const forecastResponse = await getWeatherForecast(lon, lat);
      const dailyForecast = getForcastData(forecastResponse);
      setParameters({
        ...parameters,
        weatherData: dailyForecast,
        isLoading: false,
        isError: false,
      });
    } catch {
      setParameters({ ...parameters, isLoading: false, isError: true });
    }
  };
  const getWeatheInfo = async () => {
    const { data } = await getLonLat();
    const location = data?.timezone?.split("/")[1];
    dailyForecast(location);
  };
  useEffect(() => {
    getWeatheInfo();
  }, []);

  const getSearchedLocation = (location) => {
    setParameters({ ...parameters, location: location });
  };

  const getSearchData = async () => {
    if(parameters.location){
      dailyForecast(parameters.location);
    }    
  };

  const changeUnit = () => {
    setParameters({ ...parameters, isCelsius: !parameters.isCelsius });
  };
  const getImgURL = (item) => {
    return (
      "http://openweathermap.org/img/wn/" +
      JSON.parse(JSON.stringify(item))?.weather[0]?.icon +
      ".png"
    );
  };

  return (
    <div className="weather-container">
      <div className="header">
        <div className="search-container">
          <input
            type="text"
            placeholder="Search here"
            tabIndex={0}
            onChange={(event) => getSearchedLocation(event.target.value)}
          />
          <button
            className="search-button"
            onClick={getSearchData}
            tabIndex={0}
          >
            Search
          </button>
        </div>
        <button
          className="unit"
          data-testid="unit"
          onClick={changeUnit}
          tabIndex={0}
        >
          {`${parameters.isCelsius ? "Change to °F" : "Change to °C"}`}
        </button>
      </div>
      {parameters.isLoading ? (
        <p>Loading...</p>
      ) : parameters.isError ? (
        <p>No data found</p>
      ) : (
        <div className="container-body">
          <div className="current-container">
            {parameters.weatherData?.dataForEachDay && (
              <div className="current-temp">
                <div>{parameters.weatherData?.cityName}</div>
                <div>
                  {getDate(parameters.weatherData?.dataForEachDay[0]?.dt_txt)}
                </div>
                <div className="temp">
                  <img src="getImgURL(parameters.weatherData?.dataForEachDay[0])" />
                  <div>
                    {parameters.isCelsius
                      ? `${parameters.weatherData?.dataForEachDay[0]?.main?.temp}°C`
                      : `${celsiusToFahrenheit(
                          parameters.weatherData?.dataForEachDay[0]?.main?.temp
                        )}°F`}
                  </div>
                </div>
              </div>
            )}
            {parameters.weatherData?.dataForEachDay && (
              <div className="weather-property">
                <div>
                  {`Humidity: ${parameters.weatherData?.dataForEachDay[0]?.main?.humidity}%`}
                </div>
                <div>
                  {`Wind: ${parameters.weatherData?.dataForEachDay[0]?.wind.speed} kph SW`}
                </div>
              </div>
            )}
          </div>
          <div className="forcast-container">
            {parameters.weatherData?.dataForEachDay.map((data, index) => {
              return (
                <DayCard
                  key={index}
                  data={data}
                  imgURL={getImgURL(data)}
                  isCelsius={parameters.isCelsius}
                />
              );
            })}
          </div>
        </div>
      )}
    </div>
  );
};

export default WeatherPage;
