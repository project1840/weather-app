import { celsiusToFahrenheit, getDay } from "../Util";

const DayCard = ({ data, imgURL, isCelsius }) => {
  return (
    <div>
      <div>{getDay(data?.dt_txt)}</div>
      <div>
        <img src={imgURL} />
      </div>
      <div className="temp-deg">
        {isCelsius ? (
          <div className="temp-deg">
            <div>{data?.main?.temp_max}°C</div>
            <div>{data?.main?.temp_min}°C</div>
          </div>
        ) : (
          <div className="temp-deg">
            <div>{celsiusToFahrenheit(data?.main?.temp_max)}°F</div>
            <div>{celsiusToFahrenheit(data?.main?.temp_min)}°F</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default DayCard;
