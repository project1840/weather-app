import WeatherPage from "./components/Weather";
import "./App.css";

function App() {
  return (
    <div>
      <h1 className="title">Weather App</h1>
      <WeatherPage />
    </div>
  );
}

export default App;
