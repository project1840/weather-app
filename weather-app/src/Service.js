import axios from "axios";
import * as url from "./Url";

export const getLonLat = () =>
  axios.get(`${url.LON_LAT_API}${process.env.React_App_IPINFO_API_KEY}`);

export const getWeatherForecast = (lon, lat) =>
  axios.get(
    `${url.WEATHER_FORECAST_API}lat=${lat}&lon=${lon}&units=metric&appid=${process.env.React_App_OPENWEATHER_API_KEY}`
  );

export const getCoordinates = (location) =>
  axios.get(
    `${url.COORDINATES_API.replace("location", location)}&appid=${
      process.env.React_App_OPENWEATHER_API_KEY
    }`
  );
