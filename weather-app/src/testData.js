export const mockApiData = {
    "data": {
        "cod": "200",
        "message": 0,
        "cnt": 40,
        "list": [
            {
                "dt": 1672995600,
                "main": {
                    "temp": 22.97,
                    "feels_like": 22.78,
                    "temp_min": 22.97,
                    "temp_max": 25.05,
                    "pressure": 1018,
                    "sea_level": 1018,
                    "grnd_level": 1015,
                    "humidity": 56,
                    "temp_kf": -2.08
                },
                "weather": [
                    {
                        "id": 800,
                        "main": "Clear",
                        "description": "clear sky",
                        "icon": "01d"
                    }
                ],
                "clouds": {
                    "all": 0
                },
                "wind": {
                    "speed": 4.43,
                    "deg": 341,
                    "gust": 5.03
                },
                "visibility": 10000,
                "pop": 0,
                "sys": {
                    "pod": "d"
                },
                "dt_txt": "2023-01-06 09:00:00"
            },
            {
                "dt": 1673082000,
                "main": {
                    "temp": 26.09,
                    "feels_like": 26.09,
                    "temp_min": 26.09,
                    "temp_max": 26.09,
                    "pressure": 1014,
                    "sea_level": 1014,
                    "grnd_level": 1013,
                    "humidity": 22,
                    "temp_kf": 0
                },
                "weather": [
                    {
                        "id": 800,
                        "main": "Clear",
                        "description": "clear sky",
                        "icon": "01d"
                    }
                ],
                "clouds": {
                    "all": 0
                },
                "wind": {
                    "speed": 4.14,
                    "deg": 353,
                    "gust": 4.66
                },
                "visibility": 10000,
                "pop": 0,
                "sys": {
                    "pod": "d"
                },
                "dt_txt": "2023-01-07 09:00:00"
            },
            {
                "dt": 1673168400,
                "main": {
                    "temp": 27.51,
                    "feels_like": 26.41,
                    "temp_min": 27.51,
                    "temp_max": 27.51,
                    "pressure": 1013,
                    "sea_level": 1013,
                    "grnd_level": 1012,
                    "humidity": 20,
                    "temp_kf": 0
                },
                "weather": [
                    {
                        "id": 800,
                        "main": "Clear",
                        "description": "clear sky",
                        "icon": "01d"
                    }
                ],
                "clouds": {
                    "all": 0
                },
                "wind": {
                    "speed": 4.35,
                    "deg": 358,
                    "gust": 4.89
                },
                "visibility": 10000,
                "pop": 0,
                "sys": {
                    "pod": "d"
                },
                "dt_txt": "2023-01-08 09:00:00"
            },
            {
                "dt": 1673254800,
                "main": {
                    "temp": 28.75,
                    "feels_like": 27.18,
                    "temp_min": 28.75,
                    "temp_max": 28.75,
                    "pressure": 1014,
                    "sea_level": 1014,
                    "grnd_level": 1013,
                    "humidity": 14,
                    "temp_kf": 0
                },
                "weather": [
                    {
                        "id": 800,
                        "main": "Clear",
                        "description": "clear sky",
                        "icon": "01d"
                    }
                ],
                "clouds": {
                    "all": 0
                },
                "wind": {
                    "speed": 3.32,
                    "deg": 335,
                    "gust": 3.72
                },
                "visibility": 10000,
                "pop": 0,
                "sys": {
                    "pod": "d"
                },
                "dt_txt": "2023-01-09 09:00:00"
            },
            {
                "dt": 1673341200,
                "main": {
                    "temp": 29.17,
                    "feels_like": 27.54,
                    "temp_min": 29.17,
                    "temp_max": 29.17,
                    "pressure": 1012,
                    "sea_level": 1012,
                    "grnd_level": 1011,
                    "humidity": 18,
                    "temp_kf": 0
                },
                "weather": [
                    {
                        "id": 800,
                        "main": "Clear",
                        "description": "clear sky",
                        "icon": "01d"
                    }
                ],
                "clouds": {
                    "all": 0
                },
                "wind": {
                    "speed": 4.9,
                    "deg": 321,
                    "gust": 5.88
                },
                "visibility": 10000,
                "pop": 0,
                "sys": {
                    "pod": "d"
                },
                "dt_txt": "2023-01-10 09:00:00"
            }
        ],
        "cityName": "Kolkata"
    }
}

export const dayCardMocknData = {
    "dt": 1672995600,
    "main": {
        "temp": 2.87,
        "temp_min": 2.87,
        "temp_max": 2.87,
        "pressure": 1023,
        "humidity": 63,
    },
    "weather": [
        {
            "description": "light snow",
            "icon": "13d"
        }
    ],
    "wind": {
        "speed": 6.96,
    },
    "dt_txt": "2023-01-06 09:00:00"
}

export const mockPositionByApi = {
    data: {
        coord: {
            lat: 17.1,
            lon: 79.3
        }
    }
}

export const mockCityInfo = {
    data:{
        timezone:"India/Kolkata"
    }
}