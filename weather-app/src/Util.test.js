import { getDate } from "./Util";

describe("Util functions", () => {
  it("getDay with /'nd'/", async () => {
    const result = getDate("2023-04-02 21:00:00");
    expect(result).toBe("Sunday, April 2nd");
  });
  it("getDay with /'st'/", async () => {
    const result = getDate("2023-04-01 21:00:00");
    expect(result).toBe("Saturday, April 1st");
  });
  it("getDay with /'rd'/", async () => {
    const result = getDate("2023-04-03 21:00:00");
    expect(result).toBe("Monday, April 3rd");
  });
});
