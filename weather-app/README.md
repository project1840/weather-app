# Weather App
This is Weather application

## Description
Weather Forecasting is crucial since it helps to determine future climate changes
This Weather App provides information regarding above.

## Installation
Go inside root folder (cd weather-app);
npm install;
npm start

### Unit Testing
cd weather-app;
npm run test

### Test Coverage
cd weather-app;
npm run test:coverage
